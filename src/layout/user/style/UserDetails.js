import styled from "styled-components"

export const UserDetailsWrapper = styled.div `
	display: flex;
	flex-flow: column;
	border-radius: 5px;
	border: 1px solid #c7c5c5;
	margin: 10px;
	padding: 10px;
	justify-content: center;
	text-align: center;
	user-select: none;
`
export const BackArrow = styled.div`
	display: flex;
	height: 25px;
`