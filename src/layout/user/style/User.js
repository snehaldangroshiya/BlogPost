import styled from "styled-components"

export const UserWrapper = styled.div `
	display: flex;
	border-radius: 5px;
	margin: 10px;
	padding: 10px;
	justify-content: center;;
`

export const Header = styled.h3`
	text-align: center;
`
