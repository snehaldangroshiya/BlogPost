import React from 'react';
import {UserDetailsWrapper} from "../style/UserDetails";
import {Header} from "../style/User";
import {
	faBuilding,
	faQuoteLeft,
	faThumbsUp,
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {isEmpty, isDefined} from "@snehal1112/utils";
import DisplayField from "../../../components/DisplayField";

const companyModel = [{
	name: "name",
	icon: faBuilding,
	color: "#8A8A8E",
	value:''
},{
	name: "catchPhrase",
	icon: faQuoteLeft,
	color: "#8A8A8E",
	value:''
},{
	name: "bs",
	icon: faThumbsUp,
	color: "#8A8A8E",
	value:''
}];


const UserCompanyDetails = (props) => {
	if (!isEmpty(props)) {
		for (let filed of companyModel) {
			if (isDefined(props[filed.name])) {
				filed.value = props[filed.name];
			}
		}
	}

	return (
		<UserDetailsWrapper>
			<div>
				<FontAwesomeIcon icon={faBuilding} size={"3x"} color={"#8A8A8E"}/>
			</div>
			<Header>Company details</Header>
			{
				companyModel.map(filed=>{
					return <DisplayField key={filed.name} {...filed}/>
				})
			}
		</UserDetailsWrapper>
	);
}

export default UserCompanyDetails;