import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {getUserByID} from "../../../actions/UserAction";
import {isEmpty} from "@snehal1112/utils";
import {UserWrapper} from "../style/User";
import UserDetails from "./UserDetails";

const User = (props) => {
	const {match:{params}, history, user} = props;
	useEffect(()=>{
		props.getUserByID(params.userID);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	},[params])

	return (
		<UserWrapper>
			{
				!isEmpty(user) ? (<UserDetails {...{...user, history}}/>):""
			}
		</UserWrapper>
	);
}

const mapStateToProps = state => ({
	user: state.user.item,
});

export default connect(mapStateToProps,{getUserByID})(User);