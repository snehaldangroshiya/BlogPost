import React from 'react';
import {BackArrow, UserDetailsWrapper} from "../style/UserDetails";
import {Header} from "../style/User";
import {
	faAddressCard, faArrowLeft,
	faBuilding,
	faEnvelopeOpen,
	faPassport,
	faUser
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import UserCompanyDetails from "./UserComanyDetails";
import {isEmpty, isDefined} from "@snehal1112/utils";
import DisplayField from "../../../components/DisplayField";

const userModel = [{
	name: "name",
	icon:faUser,
	color: "#8A8A8E",
	value:''
},{
	name: "username",
	icon: faPassport,
	color: "#8A8A8E",
	value:''
},{
	name: "email",
	icon: faEnvelopeOpen,
	color: "#8A8A8E",
	value:''
},{
	name: "website",
	icon: faBuilding,
	color: "#8A8A8E",
	value:''
}];

const UserDetails = (props) => {
	if (!isEmpty(props)) {
		for (let filed of userModel) {
			if (isDefined(props[filed.name])) {
				filed.value = props[filed.name];
			}
		}
	}

	const onClickBtn = () => {
		props.history.push("/posts");
	}

	return (
		<UserDetailsWrapper>
			<BackArrow style={{display:"flex"}}>
				<FontAwesomeIcon onClick={onClickBtn} style={{cursor:"pointer"}} icon={faArrowLeft} size={"2x"} color={"#8A8A8E"}/>
			</BackArrow>
			<div>
				<FontAwesomeIcon icon={faAddressCard} size={"3x"} color={"#8A8A8E"}/>
			</div>
			<Header>Personal details</Header>
			{
				userModel.map(filed => {
					return <DisplayField key={filed.name} {...filed}/>
				})
			}
			<UserCompanyDetails {...props.company}/>
		</UserDetailsWrapper>
	);
}

export default UserDetails;