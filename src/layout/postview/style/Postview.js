import styled from "styled-components"

export const Toolbar = styled.div`
	padding: 10px;
	background-color: darkslategray;
`;

export const BackBtn = styled.button`
	border-radius: 5px;
	outline: none;
	border: none;
	cursor: pointer;
`
