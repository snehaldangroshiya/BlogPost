import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {getCommentsByPostID} from "../../../actions/CommentAction";
import Post from "../../../components/post";
import {Toolbar, BackBtn} from "../style/Postview";
import PropTypes from "prop-types";
import Comment from "../../../components/comment";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Postview = (props)=> {
	const {match:{params},location:{state}, comments} = props;


	useEffect(()=>{
		props.getCommentsByPostID(params.postID);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	},[]);

	const onClickBackBtn = () => {
		props.history.push("/posts");
	}

	return (
		<div>
			<Toolbar>
				<BackBtn onClick={onClickBackBtn}>
					<FontAwesomeIcon icon={faArrowLeft} size={"2x"} color={"#8A8A8E"}/>
				</BackBtn>
			</Toolbar>
			<Post {...state}>
				<div style={{paddingTop:5}}>
					{state.body}
				</div>
			</Post>
			<div style={{padding:10}}>
				<div><b>Comments</b></div>
				{
					comments.map(comment => {
						return <Comment key={comment.id} {...comment}/>
					})
				}
			</div>
		</div>
	);
}

Postview.propTypes = {
	comments: PropTypes.array
}

const mapStateToProps = state => ({
	comments : state.comments.items
});

export default connect(mapStateToProps,{getCommentsByPostID})(Postview);