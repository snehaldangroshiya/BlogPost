import React, {useEffect} from 'react';
import Post from "../../../components/post/js/Post";
import { connect } from "react-redux";
import { getPosts } from "../../../actions/PostAction";

const PostList = (props) => {
	const {posts} = props;

	useEffect(()=>{
		props.getPosts();
	// eslint-disable-next-line react-hooks/exhaustive-deps
	},[]);
	
	const onClickPost = (showUser, post) => {
		if (showUser) {
			props.history.push(`/user/${post.userId}`);
			return;
		}
		props.history.push({
			pathname: `/posts/${post.id}`,
			state : {...post}
		});
	}

	return (
		<div>
			{
				posts.map((props) => <Post key={props.id} {...{handler: onClickPost, ...props}}/>)
			}
		</div>
	);
}

const mapStateToProps = state => ({
	posts: state.posts.items,
});

export default connect(mapStateToProps,{getPosts})(PostList);