import { combineReducers } from "redux";
import PostsReducer from "./PostsReducer";
import CommentReducer from "./CommentReducer";
import UserReducer from "./UserReducer";

export default combineReducers({
	posts: PostsReducer,
	comments: CommentReducer,
	user : UserReducer
});
