import {USER_ITEM, USER_LIST} from "../actions/action";

const initState = {
	items: [],
	item:{}
};

export default (state = initState, {type, payload}) => {
	switch (type) {
		case USER_LIST:
			return {
				...state,
				items: payload
			};
		case USER_ITEM:
			return {
				...state,
				item: payload
			}
		default:
			return state;
	}
};
