import { POST_LIST } from "../actions/action";

const initState = {
	items: []
};

export default (state = initState, action) => {
	switch (action.type) {
		case POST_LIST:
			return {
				...state,
				items: action.payload
			};
		default:
			return state;
	}
};
