import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import PostList from "./layout/postlist";

test('renders learn react link', () => {
  const { getByText } = render(<PostList />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

