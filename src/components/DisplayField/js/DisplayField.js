import React from 'react';
import {Container, IconWrapper} from "../style/DisplayField";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const DisplayField = ({icon, color, value})=> {
	return (
		<Container>
			<IconWrapper>
				<FontAwesomeIcon icon={icon} color={color}/>
			</IconWrapper>
			<span>{value}</span>
		</Container>
	);
}

export default DisplayField;