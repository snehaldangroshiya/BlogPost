import styled from "styled-components"

export const Container = styled.div `
	display: flex;
	margin: 5px;
`

export const IconWrapper = styled.span`
	width: 20px;
	margin-right: 20px;
`
