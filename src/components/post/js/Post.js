import React from 'react';
import PropTypes from 'prop-types';
import {PostWrapper, PostTitle, PostUser, IconWrapper} from "../style/Post"
import {faTag, faUser} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Post = (props) => {
	const {title, username} = props;

	const onClickPost = (event)=> {
		const {handler, ...post} = props;
		handler(false, post);
	};

	const onClickUser = (event) => {
		event.stopPropagation();
		const {handler, ...post} = props;
		handler(true, post);
	};

	return (
		<PostWrapper onClick={onClickPost}>
			<PostTitle>
				<IconWrapper>
					<FontAwesomeIcon icon={faTag} color={"#8A8A8E"}/>
				</IconWrapper>
				<span>
					{title}
				</span>
			</PostTitle>
			<PostUser id={"username"} onClick={onClickUser}>
				<IconWrapper >
					<FontAwesomeIcon icon={faUser} color={"#8A8A8E"}/>
				</IconWrapper>
				<span>
					<span>{username}</span>
				</span>
			</PostUser>
			{props.children}
		</PostWrapper>
	);
}

Post.propTypes = {
	title: PropTypes.string,
	username: PropTypes.string,
	handler: PropTypes.func,
}

Post.defaultProps = {
	title : "",
	username : "",
	handler : () => {},
}
export default Post;