import styled from "styled-components";

export const PostWrapper = styled.div `
	color: forestgreen;
    border: 1px solid;
    margin: 10px;
    font-size:small;
    padding: 10px;
    border-radius: 5px;
    cursor: pointer;
    user-select: none;
`

export const PostTitle = styled.div`
	font-size: large;
    padding: 2px 2px 0 0;	
`

export const PostUser = styled.div`
	font-size: small;
	display: inline-block;
	padding: 5px 5px 5px 0;
	min-width: 100px;
	line-height: 0.5;
`

export const IconWrapper = styled.span`
	margin-right: 5px;
`;
