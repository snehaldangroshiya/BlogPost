import React from 'react';
import {Subject, CommentWrapper, Body, Email} from "../styled/Comment";

const Comment = (props)=>{
	return (
		<CommentWrapper>
			<Subject>
				<span>{props.name}</span>
			</Subject>
			<Body>
				<span>{props.body}</span>
			</Body>
			<Email><span>{props.email}</span></Email>
		</CommentWrapper>
	);
}

export default Comment;