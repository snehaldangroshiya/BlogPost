import styled from "styled-components"

export const CommentWrapper = styled.div`
	display: flex;
	flex-flow: column;
	border: 1px solid;
	border-radius: 5px;
	margin-bottom: 10px;
`;
const BaseDiv = styled.div `
	padding: 10px;
`
export const Subject = styled(BaseDiv)`
	height: 20px;
	background-color: #3f3f3f;
	color: aliceblue;
	
`

export const Body = styled(BaseDiv)`
	font-size: small;
`

export const Email =styled(BaseDiv) `
	font-size: small;
	font-style: italic;

`

