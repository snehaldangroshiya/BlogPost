import styled from "styled-components";

export const SearchFieldWrapper = styled.div `
	position: relative;
`

export const SearchTextField = styled.input `
	padding: 0.5em 35px 0.5em 0.5em;
	outline: none;
	font-size: medium;
	border-radius: 5px;
	border: 1px solid #8a8a8e;
`
export const IconWrapper = styled.div`
    position: absolute;
    top: 8px;
    right: 16px;
    cursor: pointer;
`

export const SuggestionWrapper = styled.div`
	position: absolute;
	margin-top: 5px;
	border-radius: 3px;
	width: 100%;
	border: 1px solid #3f3f3f;
	background-color: aliceblue;
	user-select: none;
`

export const SuggestionItem = styled.div`
	padding: 5px;
	color: #3f3f3f;
	border-bottom: 1px solid #3f3f3f;
	:last-child {
		border-bottom: none
	}
`
