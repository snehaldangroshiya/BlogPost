import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {IconWrapper, SearchFieldWrapper, SearchTextField, SuggestionWrapper,SuggestionItem} from "../styled/SearchField";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import {isEmpty} from "@snehal1112/utils";
import config from "../../../config";

const SearchField = (props) => {
	const {placeholder} = props
	const [suggestions, setSuggestions]= useState([]);
	const [memoUser, setMemoUsers]= useState([]);
	const [searchText, setSearchText] = useState("");

	// eslint-disable-next-line react-hooks/exhaustive-deps
	useEffect(()=>{
		// FIXME: memoUser array empty for the first time
		// because search filed rendered first and list request for the post list
		// triggered later.
		setMemoUsers(() => getMemoUsers());
	},[])

	const getMemoUsers = () => {
		const memoUsers = window.sessionStorage.getItem(config.memoizeKeys["users"])
		return JSON.parse(memoUsers);
	}

	const onChangeSearchField = (event)=>{
		// FIXME: not a proper way.
		// get memoize users
		let users = getMemoUsers();

		const value = event.target.value;
		let suggestions = [];
		if(value.length > 0){
			const regex = new RegExp(`^${value}`, 'i');
			console.log(users)
			suggestions = users.filter(v => regex.test(v.username));
		}
		setSearchText(() => value);
		setSuggestions(() => suggestions);
	}

	const selectSearchText = (searchText)=>{
		setSearchText(() => searchText);
		setSuggestions([]);
	}

	const onClickSearch = () => {
		const user = memoUser.find((u) => u.username === searchText);
		window.location.href = `/user/${user.id}`
	}

	return (
		<SearchFieldWrapper>
			<SearchTextField
				type={"text"}
				placeholder={placeholder}
				onChange={onChangeSearchField}
				value={searchText}
			/>
			<IconWrapper onClick={onClickSearch}>
				<FontAwesomeIcon icon={faSearch} color={"#8A8A8E"}/>
			</IconWrapper>

			{
				!isEmpty(suggestions, false) ? (
					<SuggestionWrapper>
						{
							suggestions.map(sug => <SuggestionItem key={sug.id} onClick={()=>selectSearchText(sug.username)}> {sug.username} </SuggestionItem>)
						}
					</SuggestionWrapper>
				):""
			}

		</SearchFieldWrapper>
	);
}

SearchField.propTypes = {
	placeholder: PropTypes.string
}

SearchField.defaultProps = {
	placeholder : "Enter search text"
}

export default SearchField;