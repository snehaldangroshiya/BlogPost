import {COMMENT_LIST} from "./action";
import {getCommentsByPostID as fetchComments} from "../api";

export const getCommentsByPostID = (postID)=>(dispatch)=>{
	fetchComments(postID).then(comments=>dispatch({
		type : COMMENT_LIST,
		payload : comments
	}));
}