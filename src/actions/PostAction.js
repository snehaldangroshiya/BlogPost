import {POST_LIST, USER_LIST} from "./action";
import {getPosts as fetchPosts, getUsers as fetchUsers} from "../api";
import config from "../config";

export const getPosts = () => (dispatch)=>{
	fetchUsers().then((users)=>{
		const memoizeUser = users.map(({username, id})=> ({username, id}))
		window.sessionStorage.setItem(config.memoizeKeys["users"], JSON.stringify(memoizeUser))

		dispatch({
			type : USER_LIST,
			payload : users
		});

		fetchPosts().then((posts)=> {
			let data = posts.map(post=>{
				const user = users.find((user)=>{
					return user.id === post.userId;
				})
				return {...post, username : user.username}
			})
			dispatch({
				type : POST_LIST,
				payload : data
			});
		});
	});
}

