import {USER_ITEM} from "./action";
import {getUserByID as fetchUserByID} from "../api";

export const getUserByID = (userID)=>(dispatch)=>{
	fetchUserByID(userID).then(user => dispatch({
		type : USER_ITEM,
		payload : user
	}));
};