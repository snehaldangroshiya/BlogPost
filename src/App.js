import React, {Suspense} from 'react';
import {
    BrowserRouter as Router, Redirect,
    Switch,
} from "react-router-dom";
import components from "./Routers";
import Route from "./Route";
import SearchField from "./components/SearchField";

const renderLoader = () => <p>Loading</p>;

const InitApp=()=>{
    return(
        <Router>
            <Switch>
                {
                    components.map((item, index)=>{
                        return <Route key = {index} {...item}></Route>
                    })
                }
                <Redirect to={"/posts"} />
            </Switch>
        </Router>
    );
}

const App = () => {
  return (
      <div className="App">
          <Suspense fallback={renderLoader()}>
              <div style={{
                  display:"flex",
                  justifyContent:"center",
                  alignItems:"center",
                  position:"sticky",
                  top:0,
                  backgroundColor : "#3f3f3f",
                  padding: 10
              }}>
                  <SearchField placeholder={"Search users..."}/>
              </div>
              <InitApp/>
          </Suspense>
      </div>
  );
}

export default App;
