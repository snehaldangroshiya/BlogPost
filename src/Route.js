import {
    Route as Router
} from "react-router-dom";

import React from "react";

const Route = (props) => {
    return <Router {...props}/>
}

export default Route;