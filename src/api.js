import config from "./config";

const fetchData = async (url) => {
	try {
		const response = await fetch(url);
		if (!response.ok) {
			throw Error(`[${response.status}] unable to perform request`);
		}
		return  await response.json();
	} catch (e) {
		console.log(`${e} ${url}`);
	}
};

/**
 *
 * @returns {Promise<*>}
 */
export const getPosts = () => fetchData(`${config.root}/posts`);

/**
 *
 * @returns {Promise<*>}
 */
export const getPostByID = (postID) => fetchData(`${config.root}/posts/${postID}`);

/**
 *
 * @returns {Promise<*>}
 */
export const getUsers = () => fetchData(`${config.root}/users`);

/**
 *
 * @param userID
 * @returns {Promise<*>}
 */
export const getUserByID = (userID) => fetchData(`${config.root}/users/${userID}`);

/**
 *
 * @param postID
 * @returns {Promise<*>}
 */
export const getCommentsByPostID = (postID) => fetchData(`${config.root}/posts/${postID}/comments`);