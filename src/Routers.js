import {lazy} from "react";

const PostListComponent = lazy(() => import("./layout/postlist"));
const PostViewComponent = lazy(() => import("./layout/postview"));
const UserComponent = lazy(() => import("./layout/user"));

export default [{
	exact: true,
	path: "/posts/:postID",
	component: PostViewComponent
},{
	exact: true,
	path: "/posts",
	component: PostListComponent
},{
	exact: true,
	path: "/user/:userID",
	component: UserComponent
}];