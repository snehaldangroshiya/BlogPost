export default {
	root : "https://jsonplaceholder.typicode.com",
	memoizeKeys : {
		users : "MEMO_USERS"
	}
}